use super::*;
use crate::ast::{Expr, Op, Val};

use pretty_assertions::assert_eq;

#[test]
fn declarations() {
    let expr = Expr::Declaration {
        ident: "some_string",
        val: Box::new(Expr::Val(Val::Str("whatever"))),
    };
    let instrs = compile(&expr);

    assert_eq!(
        vec![
            In::Push(Val::Str("whatever")),
            In::DeclareLocal("some_string")
        ],
        instrs
    );
}

#[test]
fn assignments() {
    let expr = Expr::Assignment {
        ident: "x",
        val: Box::new(Expr::Val(Val::Num(5))),
    };
    let instrs = compile(&expr);

    assert_eq!(vec![In::Push(Val::Num(5)), In::Assign("x")], instrs);
}

#[test]
fn lists_and_tuples() {
    let expr = Expr::List(vec![Expr::Val(Val::Num(9)), Expr::Val(Val::Bool(false))]);
    let instrs = compile(&expr);

    assert_eq!(
        vec![
            In::Push(Val::Bool(false)),
            In::Push(Val::Num(9)),
            In::MakeList(2),
        ],
        instrs
    );
}

#[test]
fn shell_invocs() {
    let instrs = compile(&Expr::ShellInvoc {
        cmd: "cd",
        args: vec![Expr::Val(Val::Str(".."))],
    });

    assert_eq!(
        vec![
            In::Push(Val::Str("..")),
            In::MakeList(1),
            In::ExecShell("cd")
        ],
        instrs
    );
}

#[test]
fn env_vars() {
    let instrs = compile(&Expr::Multiple(vec![Expr::EnvVar {
        var: "EDITOR",
        fallback: Box::new(Expr::EnvVar {
            var: "EDITOR_ALT",
            fallback: Box::new(Expr::Val(Val::Str("vim"))),
        }),
    }]));

    assert_eq!(
        vec![
            In::Jump(6),
            In::Jump(4),
            In::Push(Val::Str("vim")),
            In::Jump(5),
            In::EnvLookup("EDITOR_ALT", 2),
            In::Jump(7),
            In::EnvLookup("EDITOR", 1),
        ],
        instrs
    );
}

#[test]
fn unary_exprs() {
    let instrs = compile(&Expr::Un {
        op: Op::Neg,
        expr: Box::new(Expr::Lookup("ident")),
    });

    assert_eq!(vec![In::Lookup("ident"), In::UnOp(Op::Neg),], instrs);
}

#[test]
fn bin_exprs() {
    let instrs = compile(&Expr::Bin {
        l: Box::new(Expr::Bin {
            l: Box::new(Expr::Val(Val::Num(58))),
            op: Op::Mul,
            r: Box::new(Expr::Val(Val::Float(3.49))),
        }),
        op: Op::Sub,
        r: Box::new(Expr::Val(Val::Num(-4))),
    });

    assert_eq!(
        vec![
            In::Push(Val::Num(58)),
            In::Push(Val::Float(3.49)),
            In::Op(Op::Mul),
            In::Push(Val::Num(-4)),
            In::Op(Op::Sub),
        ],
        instrs
    );
}

#[test]
fn ident_lookups() {
    let instrs = compile(&Expr::Lookup("ident"));

    assert_eq!(In::Lookup("ident"), instrs[0]);
}

#[test]
fn anon_scopes() {
    let instrs = compile(&Expr::Scope(vec![
        Expr::Declaration {
            ident: "g",
            val: Box::new(Expr::Val(Val::Bool(false))),
        },
        Expr::Scope(vec![Expr::Declaration {
            ident: "g",
            val: Box::new(Expr::Un {
                op: Op::Neg,
                expr: Box::new(Expr::Lookup("g")),
            }),
        }]),
    ]));

    assert_eq!(
        vec![
            In::Begin,
            In::Push(Val::Bool(false)),
            In::DeclareLocal("g"),
            In::Begin,
            In::Lookup("g"),
            In::UnOp(Op::Neg),
            In::DeclareLocal("g"),
            In::End,
            In::End,
        ],
        instrs
    );
}

#[test]
fn if_exprs() {
    let instrs = compile(&Expr::If {
        branches: vec![
            (
                Expr::Lookup("cond1"),
                Expr::Bin {
                    l: Box::new(Expr::Val(Val::Num(5))),
                    op: Op::Sub,
                    r: Box::new(Expr::Val(Val::Num(9))),
                },
            ),
            (
                Expr::Bin {
                    l: Box::new(Expr::Lookup("x")),
                    op: Op::NE,
                    r: Box::new(Expr::Val(Val::Num(9))),
                },
                Expr::Val(Val::Bool(false)),
            ),
        ],
        fallback: Some(Box::new(Expr::Lookup("y"))),
    });

    assert_eq!(
        vec![
            In::Lookup("cond1"),
            In::JumpIfNot(6),
            In::Push(Val::Num(5)),
            In::Push(Val::Num(9)),
            In::Op(Op::Sub),
            In::Jump(13),
            In::Lookup("x"),
            In::Push(Val::Num(9)),
            In::Op(Op::NE),
            In::JumpIfNot(12),
            In::Push(Val::Bool(false)),
            In::Jump(13),
            In::Lookup("y"),
        ],
        instrs
    );
}

#[test]
fn if_expr_without_fallback() {
    let instrs = compile(&Expr::If {
        branches: vec![(Expr::Lookup("cond"), Expr::Val(Val::Num(1)))],
        fallback: None,
    });

    assert_eq!(
        vec![
            In::Lookup("cond"),
            In::JumpIfNot(4),
            In::Push(Val::Num(1)),
            In::Jump(5),
            In::Push(Val::Unit)
        ],
        instrs
    );
}

#[test]
fn if_with_scopes() {
    let instrs = compile(&Expr::Multiple(vec![
        Expr::Declaration {
            ident: "some_cond",
            val: Box::new(Expr::Val(Val::Bool(false))),
        },
        Expr::If {
            branches: vec![(
                Expr::Lookup("some_cond"),
                Expr::Scope(vec![Expr::ShellInvoc {
                    cmd: "echo",
                    args: vec![Expr::Val(Val::Str("tr"))],
                }]),
            )],
            fallback: Some(Box::new(Expr::Scope(vec![Expr::ShellInvoc {
                cmd: "echo",
                args: vec![Expr::Val(Val::Str("fs"))],
            }]))),
        },
    ]));

    assert_eq!(
        vec![
            In::Push(Val::Bool(false)),
            In::DeclareLocal("some_cond"),
            In::Lookup("some_cond"),
            In::JumpIfNot(10),
            In::Begin,
            In::Push(Val::Str("tr")),
            In::MakeList(1),
            In::ExecShell("echo"),
            In::End,
            In::Jump(15),
            In::Begin,
            In::Push(Val::Str("fs")),
            In::MakeList(1),
            In::ExecShell("echo"),
            In::End,
        ],
        instrs
    );
}

#[test]
fn function_declarations() {
    let instrs = compile(&Expr::FuncDef {
        ident: "sum",
        params: vec!["x", "y"],
        body: vec![
            Expr::Declaration {
                ident: "res",
                val: Box::new(Expr::Bin {
                    l: Box::new(Expr::Lookup("x")),
                    op: Op::Add,
                    r: Box::new(Expr::Lookup("y")),
                }),
            },
            Expr::Lookup("res"),
        ],
    });

    assert_eq!(
        vec![
            In::Jump(11),
            In::Begin,
            In::DeclareLocal("y"),
            In::DeclareLocal("x"),
            In::Lookup("x"),
            In::Lookup("y"),
            In::Op(Op::Add),
            In::DeclareLocal("res"),
            In::Lookup("res"),
            In::End,
            In::Ret,
            In::MakeFunction(1, 2),
            In::DeclareLocal("sum"),
        ],
        instrs
    );
}

#[test]
fn lambdas() {
    let instrs = compile(&Expr::Lambda {
        params: vec!["x", "y"],
        body: vec![
            Expr::Declaration {
                ident: "res",
                val: Box::new(Expr::Bin {
                    l: Box::new(Expr::Lookup("x")),
                    op: Op::Add,
                    r: Box::new(Expr::Lookup("y")),
                }),
            },
            Expr::Lookup("res"),
        ],
    });

    assert_eq!(
        vec![
            In::Jump(11),
            In::Begin,
            In::DeclareLocal("y"),
            In::DeclareLocal("x"),
            In::Lookup("x"),
            In::Lookup("y"),
            In::Op(Op::Add),
            In::DeclareLocal("res"),
            In::Lookup("res"),
            In::End,
            In::Ret,
            In::MakeFunction(1, 2),
        ],
        instrs
    );
}

#[test]
fn hofs() {
    let instrs = compile(&Expr::Multiple(vec![
        Expr::FuncDef {
            ident: "f",
            params: vec!["g"],
            body: vec![Expr::Call {
                ident: "g",
                args: vec![],
            }],
        },
        Expr::Call {
            ident: "f",
            args: vec![Expr::Lambda {
                params: vec![],
                body: vec![Expr::Val(Val::Unit)],
            }],
        },
    ]));

    assert_eq!(
        vec![
            In::Jump(7),
            In::Begin,
            In::DeclareLocal("g"),
            In::Push(Val::Addr(5)),
            In::Call("g", 0),
            In::End,
            In::Ret,
            In::MakeFunction(1, 1),
            In::DeclareLocal("f"),
            In::Push(Val::Addr(17)),
            In::Jump(15),
            In::Begin,
            In::Push(Val::Unit),
            In::End,
            In::Ret,
            In::MakeFunction(11, 0),
            In::Call("f", 1),
        ],
        instrs
    );
}

#[test]
fn void_function_declarations() {
    let instrs = compile(&Expr::FuncDef {
        ident: "f",
        params: vec![],
        body: vec![Expr::Declaration {
            ident: "x",
            val: Box::new(Expr::Val(Val::Num(0))),
        }],
    });

    assert_eq!(
        vec![
            In::Jump(7),
            In::Begin,
            In::Push(Val::Num(0)),
            In::DeclareLocal("x"),
            In::Push(Val::Unit),
            In::End,
            In::Ret,
            In::MakeFunction(1, 0),
            In::DeclareLocal("f"),
        ],
        instrs
    );
}

#[test]
fn function_calling() {
    let instrs = compile(&Expr::Declaration {
        ident: "x",
        val: Box::new(Expr::Call {
            ident: "sum",
            args: vec![Expr::Lookup("x"), Expr::Val(Val::Num(9))],
        }),
    });

    assert_eq!(
        vec![
            In::Push(Val::Addr(4)),
            In::Lookup("x"),
            In::Push(Val::Num(9)),
            In::Call("sum", 2),
            In::DeclareLocal("x"),
        ],
        instrs
    )
}

#[test]
fn range_loops() {
    let instrs = compile(&Expr::RangeLoop {
        ident: "idx",
        from: Box::new(Expr::Val(Val::Num(0))),
        down: false,
        to: Box::new(Expr::Val(Val::Num(2))),
        inc: true,
        by: Box::new(Expr::Val(Val::Num(2))),
        body: vec![Expr::Assignment {
            ident: "x",
            val: Box::new(Expr::Lookup("idx")),
        }],
    });

    assert_eq!(
        vec![
            In::Begin,
            In::Push(Val::Num(0)),
            In::DeclareLocal("idx"),
            In::Push(Val::Num(2)),
            In::DeclareLocal("___to"),
            In::Push(Val::Num(2)),
            In::DeclareLocal("___by"),
            In::Lookup("idx"),
            In::Lookup("___to"),
            In::Op(Op::LE),
            In::JumpIfNot(18),
            In::Lookup("idx"),
            In::Assign("x"),
            In::Lookup("idx"),
            In::Lookup("___by"),
            In::Op(Op::Add),
            In::Assign("idx"),
            In::Jump(7),
            In::End,
        ],
        instrs
    )
}

#[test]
fn range_loops_down_to() {
    let instrs = compile(&Expr::RangeLoop {
        ident: "idx",
        from: Box::new(Expr::Val(Val::Num(2))),
        down: true,
        to: Box::new(Expr::Val(Val::Num(0))),
        inc: false,
        by: Box::new(Expr::Val(Val::Num(-1))),
        body: vec![Expr::Assignment {
            ident: "x",
            val: Box::new(Expr::Lookup("idx")),
        }],
    });

    assert_eq!(
        vec![
            In::Begin,
            In::Push(Val::Num(2)),
            In::DeclareLocal("idx"),
            In::Push(Val::Num(0)),
            In::DeclareLocal("___to"),
            In::Push(Val::Num(-1)),
            In::DeclareLocal("___by"),
            In::Lookup("idx"),
            In::Lookup("___to"),
            In::Op(Op::GT),
            In::JumpIfNot(18),
            In::Lookup("idx"),
            In::Assign("x"),
            In::Lookup("idx"),
            In::Lookup("___by"),
            In::Op(Op::Add),
            In::Assign("idx"),
            In::Jump(7),
            In::End,
        ],
        instrs
    )
}

#[test]
fn factorial() {
    let instrs = compile(&Expr::FuncDef {
        ident: "fact",
        params: vec!["x"],
        body: vec![Expr::If {
            branches: vec![
                (
                    Expr::Bin {
                        l: Box::new(Expr::Lookup("x")),
                        op: Op::LT,
                        r: Box::new(Expr::Val(Val::Num(0))),
                    },
                    Expr::Val(Val::Unit),
                ),
                (
                    Expr::Bin {
                        l: Box::new(Expr::Lookup("x")),
                        op: Op::LT,
                        r: Box::new(Expr::Val(Val::Num(2))),
                    },
                    Expr::Val(Val::Num(1)),
                ),
            ],
            fallback: Some(Box::new(Expr::Bin {
                l: Box::new(Expr::Lookup("x")),
                op: Op::Mul,
                r: Box::new(Expr::Call {
                    ident: "fact",
                    args: vec![Expr::Bin {
                        l: Box::new(Expr::Lookup("x")),
                        op: Op::Sub,
                        r: Box::new(Expr::Val(Val::Num(1))),
                    }],
                }),
            })),
        }],
    });

    assert_eq!(
        vec![
            In::Jump(24),
            In::Begin,
            In::DeclareLocal("x"),
            // if x < 0
            In::Lookup("x"),
            In::Push(Val::Num(0)),
            In::Op(Op::LT),
            In::JumpIfNot(9),
            In::Push(Val::Unit),
            In::Jump(22),
            // if x < 2
            In::Lookup("x"), // 9
            In::Push(Val::Num(2)),
            In::Op(Op::LT),
            In::JumpIfNot(15),
            In::Push(Val::Num(1)),
            In::Jump(22),
            // else
            In::Lookup("x"), // 15
            In::Push(Val::Addr(21)),
            In::Lookup("x"),
            In::Push(Val::Num(1)),
            In::Op(Op::Sub),
            In::Call("fact", 1),
            In::Op(Op::Mul), // 21
            In::End,         // 22
            In::Ret,
            In::MakeFunction(1, 1),
            In::DeclareLocal("fact"),
        ],
        instrs
    );
}
