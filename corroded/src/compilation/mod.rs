use crate::{
    ast::{Expr, Op, Val},
    vm::instructions::In,
};

#[cfg(test)]
mod tests;

fn compile_multiple<'t>(v: &[Expr<'t>]) -> Vec<In<'t>> {
    let mut vec = Vec::new();
    v.iter().for_each(|e| compile_single(e, &mut vec));
    vec
}

/// Compiles an expression to bytecode.
pub fn compile<'t>(expr: &Expr<'t>) -> Vec<In<'t>> {
    let mut v = Vec::new();
    compile_single(expr, &mut v);
    v
}

// TODO: is it possible to consume the `Expr` ?
fn compile_single<'t>(expr: &Expr<'t>, vec: &mut Vec<In<'t>>) {
    // helpers
    let offset_instr = |i, offset: usize| match i {
        In::Jump(x) => In::Jump(offset + x),
        In::JumpIf(x) => In::JumpIf(offset + x),
        In::JumpIfNot(x) => In::JumpIfNot(offset + x),
        In::Push(Val::Addr(x)) => In::Push(Val::Addr(offset + x)),
        In::EnvLookup(var, x) => In::EnvLookup(var, x + offset),
        In::MakeFunction(addr, params) => In::MakeFunction(addr + offset, params),
        _ => i,
    };

    let push_by_offset = |ins: &[In<'t>], vec: &mut Vec<In<'t>>, offset: usize| {
        for i in ins {
            vec.push(offset_instr(*i, offset));
        }
    };

    let compile_function = |params: &[&'t str], body: &[Expr<'t>], vec: &mut Vec<In<'t>>| {
        let mut body_v = Vec::new();
        body.iter()
            .for_each(|expr| compile_single(expr, &mut body_v));

        // if the last expression in the body is something "void",
        // push a Unit onto the stack as the function's result value
        let is_void_result = match body.last().unwrap() {
            // TODO: fill in all the exprs
            Expr::Declaration { .. } => true,
            _ => false,
        };

        let fn_start = vec.len() + 1;
        let after_function =
            fn_start + body_v.len() + params.len() + if is_void_result { 1 } else { 0 } + 3;

        vec.push(In::Jump(after_function));
        vec.push(In::Begin);
        params
            .iter()
            .rev()
            .for_each(|p| vec.push(In::DeclareLocal(p)));

        push_by_offset(&body_v, vec, fn_start + params.len() + 1);

        if is_void_result {
            vec.push(In::Push(Val::Unit));
        }

        vec.push(In::End);
        vec.push(In::Ret);
        vec.push(In::MakeFunction(fn_start, params.len()));
    };

    // actual logic
    match expr {
        Expr::Multiple(exprs) => exprs.iter().for_each(|expr| compile_single(expr, vec)),
        Expr::Scope(exprs) => {
            vec.push(In::Begin);
            exprs.iter().for_each(|expr| compile_single(expr, vec));
            vec.push(In::End);
        }
        Expr::Un { op, expr } => {
            compile_single(expr, vec);
            vec.push(In::UnOp(*op));
        }
        Expr::Bin { l, op, r } => {
            compile_single(l, vec);
            compile_single(r, vec);
            vec.push(In::Op(*op));
        }
        Expr::Val(v) => vec.push(In::Push(*v)),
        Expr::List(xs) => {
            // list items are pushed onto the stack in reverse as their
            // order needs to be preserved when the list is actually created
            xs.iter().rev().for_each(|x| compile_single(x, vec));
            vec.push(In::MakeList(xs.len()));
        }
        Expr::Declaration { ident, val } => {
            compile_single(val, vec);
            vec.push(In::DeclareLocal(ident));
        }
        Expr::FuncDef {
            ident,
            params,
            body,
        } => {
            compile_function(params, body, vec);
            vec.push(In::DeclareLocal(ident));
        }
        Expr::Lambda { params, body } => compile_function(params, body, vec),
        Expr::Call { ident, args } => {
            let mut args_v = Vec::new();
            args.iter().for_each(|arg| compile_single(arg, &mut args_v));

            // + 2 because of the Push(addr) and Call
            let after_call = vec.len() + args_v.len() + 2;

            vec.push(In::Push(Val::Addr(after_call)));
            push_by_offset(&args_v, vec, vec.len());
            vec.push(In::Call(ident, args.len()));
        }
        Expr::NativeCall { ident, args } => {
            let mut args_v = compile_multiple(args);

            vec.append(&mut args_v);
            vec.push(In::CallNative(ident, args.len()));
        }
        Expr::RangeLoop {
            ident,
            from,
            down,
            to,
            inc,
            by,
            body,
        } => {
            vec.push(In::Begin);
            compile_single(from, vec);
            vec.push(In::DeclareLocal(ident));
            compile_single(to, vec);
            vec.push(In::DeclareLocal("___to"));
            compile_single(by, vec);
            vec.push(In::DeclareLocal("___by"));

            let body_v = compile_multiple(body);
            let loop_start = vec.len();
            let loop_end = loop_start + body_v.len() + 9;

            vec.push(In::Lookup(ident)); // loop_start
            vec.push(In::Lookup("___to"));
            vec.push(In::Op(match (down, inc) {
                (true, true) => Op::GE,
                (true, _) => Op::GT,
                (_, true) => Op::LE,
                _ => Op::LT,
            }));
            vec.push(In::JumpIfNot(loop_end));
            push_by_offset(&body_v, vec, vec.len());
            vec.push(In::Lookup(ident));
            vec.push(In::Lookup("___by"));
            vec.push(In::Op(Op::Add));
            vec.push(In::Assign(ident));
            vec.push(In::Jump(loop_start));
            vec.push(In::End); // loop_end
        }
        Expr::Assignment { ident, val } => {
            compile_single(val, vec);
            vec.push(In::Assign(ident));
        }
        Expr::ShellInvoc { cmd, args } => {
            compile_single(&Expr::List(args.to_vec()), vec);
            vec.push(In::ExecShell(cmd));
        }
        Expr::Lookup(ident) => vec.push(In::Lookup(ident)),
        Expr::EnvVar { var, fallback } => {
            let alt_ins = compile(fallback);

            let offset = vec.len() + 1;

            vec.reserve(alt_ins.len() + 3);
            vec.push(In::Jump(alt_ins.len() + 2));

            push_by_offset(&alt_ins, vec, offset);

            vec.push(In::Jump(vec.len() + 2));
            vec.push(In::EnvLookup(var, offset));
        }
        Expr::If { branches, fallback } => {
            let mut offset = vec.len();
            let mut bs = Vec::new();

            for (cond, expr) in branches {
                let mut cond_v = compile(cond);
                let mut expr_v = compile(expr);

                offset += 2 + cond_v.len() + expr_v.len();

                bs.append(&mut cond_v);
                bs.push(In::JumpIfNot(offset));
                bs.append(&mut expr_v);
                bs.push(In::Jump(0));
            }

            if let Some(fallback) = fallback {
                compile_single(fallback, &mut bs);
            } else {
                bs.push(In::Push(Val::Unit));
            }

            offset = bs.len() + vec.len();

            for i in bs {
                vec.push(match i {
                    In::Jump(0) => In::Jump(offset),
                    _ => i,
                });
            }
        }
        _ => {
            dbg!(expr);
            panic!("Unsupported expression!");
        }
    }
}
