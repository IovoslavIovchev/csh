#[derive(Clone, Debug, PartialEq)]
pub enum Expr<'t> {
    Multiple(Vec<Expr<'t>>),
    Scope(Vec<Expr<'t>>),
    Use {
        name: &'t str,
        alias: &'t str,
    },
    Declaration {
        ident: &'t str,
        val: Box<Expr<'t>>,
    },
    FuncDef {
        ident: &'t str,
        params: Vec<&'t str>,
        body: Vec<Expr<'t>>,
    },
    Lambda {
        params: Vec<&'t str>,
        body: Vec<Expr<'t>>,
    },
    Call {
        ident: &'t str,
        args: Vec<Expr<'t>>,
    },
    NativeCall {
        ident: &'t str,
        args: Vec<Expr<'t>>,
    },
    RangeLoop {
        ident: &'t str,
        from: Box<Expr<'t>>,
        down: bool,
        to: Box<Expr<'t>>,
        inc: bool,
        by: Box<Expr<'t>>,
        body: Vec<Expr<'t>>,
    },
    ForLoop(&'t str, Option<&'t str>, Box<Expr<'t>>, Vec<Expr<'t>>),
    Assignment {
        ident: &'t str,
        val: Box<Expr<'t>>,
    },
    Un {
        op: Op,
        expr: Box<Expr<'t>>,
    },
    Bin {
        l: Box<Expr<'t>>,
        op: Op,
        r: Box<Expr<'t>>,
    },
    Lookup(&'t str),
    ChainVal,
    Val(Val<'t>),
    List(Vec<Expr<'t>>),
    If {
        branches: Vec<(Expr<'t>, Expr<'t>)>,
        fallback: Option<Box<Expr<'t>>>,
    },
    ShellInvoc {
        cmd: &'t str,
        args: Vec<Expr<'t>>,
    },
    EnvVar {
        var: &'t str,
        fallback: Box<Expr<'t>>,
    },
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Val<'t> {
    Unit,
    Bool(bool),
    Num(i32),
    Float(f64),
    Str(&'t str),
    Addr(usize),
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Op {
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Pow,
    And,
    Neg,
    Chain,
    At,
    Or,
    EQ,
    NE,
    LT,
    LE,
    GT,
    GE,
}

#[derive(Debug, PartialEq)]
pub enum Keyword {
    Function,
    If,

    // a currently unused but reserved keyword
    Reserved,
}
