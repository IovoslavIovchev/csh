#[macro_use]
extern crate lalrpop_util;
lalrpop_mod!(pub grammar);

mod ast;
mod compilation;
mod parsing;
mod vm;

pub use vm::{VMErr, VMVal, VM};

pub fn compile_src(src: &'_ str) -> Result<Vec<vm::instructions::Instruction<'_>>, &'static str> {
    let expr = parsing::parse(src)?;
    Ok(compilation::compile(&expr))
}

/// Transforms a `VMErr` into a pretty-printed human-readable error.
/// Consumes the `err`.
pub fn prettify_err(err: VMErr) -> String {
    use ast::Op;
    use VMErr::*;
    let err = match err {
        InvalidUnOp(op, t) => {
            let op = match op {
                Op::Neg => "negate",
                _ => return "Unknown unary operation..".to_string(),
            };

            format!("Cannot {} a `{}`", op, t)
        }
        InvalidBinOp(l_type, op, r_type) => {
            let op = match op {
                Op::Add => "addition",
                _ => return "Unknown binary operation..".to_string(),
            };

            format!(
                "Performing {} on types `{}` and `{}` is unsupported.",
                op, l_type, r_type
            )
        }
        UnresolvedIdent(ident) => format!("Cannot find value `{}` in the current scope", ident),
        _ => "Unspecified error..".to_string(),
    };

    format!("[Err] {}", err)
}
