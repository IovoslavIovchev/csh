use crate::ast::{Op, Val};

// Laziness.. that's why
pub(crate) type In<'t> = Instruction<'t>;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Instruction<'t> {
    Begin,
    End,
    Push(Val<'t>),
    UnOp(Op),
    Op(Op),
    Jump(usize),
    /// Jump to the address if TOS is `true`
    JumpIf(usize),
    /// Jump to the address if TOS is `false`
    JumpIfNot(usize),
    Ret,
    Call(&'t str, usize),
    CallNative(&'t str, usize),
    MakeList(usize),
    MakeFunction(usize, usize),
    DeclareLocal(&'t str),
    Assign(&'t str),
    ExecShell(&'t str),
    EnvLookup(&'t str, usize),
    Lookup(&'t str),
}
