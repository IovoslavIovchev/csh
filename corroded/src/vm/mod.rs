pub mod instructions;

use crate::ast::{Op, Val};
use instructions::In;
use std::collections::BTreeMap;

#[cfg(test)]
mod tests;

// FIXME: ParialOrd yields false if the the discriminants differ
#[derive(Clone, Debug, PartialEq, PartialOrd)]
pub enum VMVal {
    Unit,
    Bool(bool),
    Num(i32),
    Addr(usize),
    Float(f64),
    Str(String),
    List(Vec<VMVal>),

    /// Fn(addr, params_len)
    Fn(usize, usize),
}

impl VMVal {
    fn type_str(&self) -> &'static str {
        use VMVal::*;
        match self {
            Unit => "Unit",
            Bool(_) => "Boolean",
            Num(_) => "Number",
            Addr(_) => "Address",
            Float(_) => "Float",
            Str(_) => "String",
            List(_) => "List",
            _ => unimplemented!(),
        }
    }
}

impl From<Val<'_>> for VMVal {
    fn from(val: Val) -> Self {
        match val {
            Val::Unit => VMVal::Unit,
            Val::Bool(x) => VMVal::Bool(x),
            Val::Num(x) => VMVal::Num(x),
            Val::Addr(x) => VMVal::Addr(x),
            Val::Float(x) => VMVal::Float(x),
            Val::Str(s) => VMVal::Str(s.to_string()),
            _ => unimplemented!(),
        }
    }
}

impl ToString for VMVal {
    fn to_string(&self) -> String {
        match self {
            VMVal::Unit => "()".to_string(),
            VMVal::Bool(x) => x.to_string(),
            VMVal::Num(x) => x.to_string(),
            VMVal::Float(x) => x.to_string(),
            VMVal::Str(s) => s.clone(),
            VMVal::List(xs) => format!(
                "[ {} ]",
                xs.iter()
                    .map(|x| x.to_string())
                    .collect::<Vec<_>>()
                    .join(", ")
            ),
            _ => unimplemented!(),
        }
    }
}

#[derive(Debug, Default, PartialEq)]
struct Scope<'t> {
    bindings: BTreeMap<&'t str, VMVal>,
}

impl<'t> Scope<'t> {
    fn bind(&mut self, ident: &'t str, val: VMVal) {
        self.bindings.insert(ident, val);
    }

    fn lookup(&self, ident: &'t str) -> Option<&VMVal> {
        self.bindings.get(ident)
    }
}

#[derive(Debug, Default)]
pub struct VM<'t> {
    ptr: usize,
    stack: Vec<VMVal>,
    call_stack: Vec<Scope<'t>>,
}

/// Represents the various types of errors that might occur during VM execution
#[derive(Debug, PartialEq)]
pub enum VMErr {
    /// Expected type `.0` but got `.1`
    IncorrectType(&'static str, &'static str),
    /// An unresolved identifier
    UnresolvedIdent(String),
    /// Invalid unary operation `.0` on type `.1`
    InvalidUnOp(Op, &'static str),
    /// Invalid binary operation `.1` between type `.0` and type `.2`
    InvalidBinOp(&'static str, Op, &'static str),
    /// Function `.0` expected `.1` args but was passed `.2`
    WrongNumOfArgs(String, usize, usize),
    /// Currently unsupported OS to run shell commands on
    UnsupportedOs,
    /// An out of bound indexing was performed
    OutOfBoundsIndexing,
    /// A list was indexed by a negative number
    NegativeIndexing,
    Generic,
}

impl<'t> VM<'t> {
    pub fn new() -> Self {
        let mut s = Self::default();
        // the initial "global" scope
        s.call_stack.push(Scope::default());
        s
    }

    fn push(&mut self, val: VMVal) {
        self.stack.push(val);
    }

    fn pop(&mut self) -> VMVal {
        self.stack.pop().unwrap()
    }

    fn bind(&mut self, ident: &'t str, val: VMVal) {
        self.call_stack.last_mut().unwrap().bind(ident, val);
    }

    fn assign(&mut self, ident: &'t str, val: VMVal) -> Result<(), VMErr> {
        // TODO: please find a more declarative way to rewrite this
        for s in self.call_stack.iter_mut().rev() {
            if s.bindings.contains_key(ident) {
                s.bind(ident, val);
                return Ok(());
            }
        }

        Err(VMErr::UnresolvedIdent(ident.to_string()))
    }

    /// Gets the value of `ident` as it appears in the global scope.
    pub fn get_value(&self, ident: &'t str) -> Option<&VMVal> {
        self.call_stack[0].lookup(ident)
    }

    fn lookup(&self, ident: &'t str) -> Result<&VMVal, VMErr> {
        // TODO: find a more declarative way to rewrite this
        for f in self.call_stack.iter().rev() {
            if let Some(val) = f.lookup(ident) {
                return Ok(val);
            }
        }

        Err(VMErr::UnresolvedIdent(ident.to_string()))
    }

    // === Natives ===
    fn log(&mut self) {
        let val = self.pop();
        println!("{}", val.to_string());
    }

    fn dbg(&mut self) {
        let val = self.pop();
        println!("[DEBUG] {:?}", val);
    }

    fn type_of(&mut self) {
        let t = self.pop().type_str();
        self.push(VMVal::Str(t.into()));
    }

    fn len(&mut self) -> Result<(), VMErr> {
        let len = match self.pop() {
            VMVal::List(xs) => xs.len(),
            // TODO: custom err
            _ => return Err(VMErr::Generic),
        };

        self.push(VMVal::Num(len as i32));

        Ok(())
    }

    fn get_args(&mut self) {
        // TODO: can this be optimised ?
        let args = std::env::args().map(|arg| VMVal::Str(arg)).collect();
        self.push(VMVal::List(args));
    }

    fn zip(&mut self) -> Result<(), VMErr> {
        let (ys, xs) = (self.pop(), self.pop());

        use VMVal::*;
        match (xs, ys) {
            (List(xs), List(ys)) => {
                let zs = xs
                    .iter()
                    .zip(ys)
                    .map(|(x, y)| List(vec![x.clone(), y.clone()]))
                    .collect();
                self.push(List(zs));
            }
            _ => return Err(VMErr::Generic),
        }

        Ok(())
    }

    fn lines(&mut self) -> Result<(), VMErr> {
        let lns = match self.pop() {
            VMVal::Str(s) => s.lines().map(|ln| VMVal::Str(ln.to_owned())).collect(),
            _ => return Err(VMErr::Generic),
        };

        self.push(VMVal::List(lns));

        Ok(())
    }

    pub fn exec(&mut self, instrs: &[In<'t>]) -> Result<(), VMErr> {
        self.ptr = 0;

        while self.ptr < instrs.len() {
            match instrs[self.ptr] {
                In::Begin => {
                    self.call_stack.push(Default::default());
                }
                In::End => {
                    self.call_stack.pop();
                }
                In::Push(val) => {
                    self.push(VMVal::from(val));
                }
                In::UnOp(op) => {
                    let tos = self.pop();

                    let t = tos.type_str();

                    use Op::*;
                    use VMVal::*;
                    let tos = match (op, tos) {
                        (Neg, Bool(x)) => Bool(!x),
                        _ => return Err(VMErr::InvalidUnOp(op, t)),
                    };

                    self.push(tos);
                }
                // TODO: implement the ops as VMVal traits
                In::Op(op) => {
                    let (y, x) = (self.pop(), self.pop());

                    // get the types beforehand as `x` and `y` get moved
                    let (x_t, y_t) = (x.type_str(), y.type_str());

                    use Op::*;
                    use VMVal::*;
                    let z = match op {
                        Add => match (x, y) {
                            (Num(x), Num(y)) => Num(x + y),
                            (Float(x), Float(y)) => Float(x + y),
                            (Num(x), Float(y)) => Float(x as f64 + y),
                            (Float(x), Num(y)) => Float(x + y as f64),
                            (Str(s), Num(x)) => Str(s + &x.to_string()),
                            (Str(s), Float(x)) => Str(s + &x.to_string()),
                            (Str(x), Str(y)) => Str(x + &y),
                            _ => return Err(VMErr::InvalidBinOp(x_t, op, y_t)),
                        },
                        Sub => match (x, y) {
                            (Num(x), Num(y)) => Num(x - y),
                            (Float(x), Float(y)) => Float(x - y),
                            (Num(x), Float(y)) => Float(x as f64 - y),
                            (Float(x), Num(y)) => Float(x - y as f64),
                            _ => return Err(VMErr::InvalidBinOp(x_t, op, y_t)),
                        },
                        Mul => match (x, y) {
                            (Num(x), Num(y)) => Num(x * y),
                            (Float(x), Float(y)) => Float(x * y),
                            (Num(x), Float(y)) => Float(x as f64 * y),
                            (Float(x), Num(y)) => Float(x * y as f64),
                            _ => return Err(VMErr::InvalidBinOp(x_t, op, y_t)),
                        },
                        Div => match (x, y) {
                            (Num(x), Num(y)) => Num(x / y),
                            (Float(x), Float(y)) => Float(x / y),
                            (Num(x), Float(y)) => Float(x as f64 / y),
                            (Float(x), Num(y)) => Float(x / y as f64),
                            _ => return Err(VMErr::InvalidBinOp(x_t, op, y_t)),
                        },
                        And => match (x, y) {
                            (Bool(x), Bool(y)) => Bool(x && y),
                            _ => return Err(VMErr::InvalidBinOp(x_t, op, y_t)),
                        },
                        Or => match (x, y) {
                            (Bool(x), Bool(y)) => Bool(x || y),
                            _ => return Err(VMErr::InvalidBinOp(x_t, op, y_t)),
                        },
                        At => match (x, y) {
                            (List(xs), Num(idx)) if idx > -1 => match xs.get(idx as usize) {
                                Some(val) => val.clone(),
                                _ => return Err(VMErr::OutOfBoundsIndexing),
                            },
                            (List(_), Num(_)) => return Err(VMErr::NegativeIndexing),
                            _ => return Err(VMErr::InvalidBinOp(x_t, op, y_t)),
                        },
                        EQ => Bool(x == y),
                        NE => Bool(x != y),
                        GT => Bool(x > y),
                        GE => Bool(x >= y),
                        LT => Bool(x < y),
                        LE => Bool(x <= y),
                        _ => {
                            dbg!(op);
                            panic!("Unsupported Op");
                        }
                    };

                    self.push(z);
                }
                In::Jump(addr) => {
                    self.ptr = addr;
                    continue;
                }
                In::JumpIf(addr) => {
                    let tos = self.pop();

                    match tos {
                        VMVal::Bool(true) => {
                            self.ptr = addr;
                            continue;
                        }
                        VMVal::Bool(_) => (),
                        _ => {
                            return Err(VMErr::IncorrectType(
                                VMVal::Bool(false).type_str(),
                                tos.type_str(),
                            ))
                        }
                    };
                }
                // TODO: remove duplication
                In::JumpIfNot(addr) => {
                    let tos = self.pop();

                    match tos {
                        VMVal::Bool(false) => {
                            self.ptr = addr;
                            continue;
                        }
                        VMVal::Bool(_) => (),
                        _ => {
                            return Err(VMErr::IncorrectType(
                                VMVal::Bool(false).type_str(),
                                tos.type_str(),
                            ))
                        }
                    };
                }
                In::Ret => {
                    let addr = self.stack.remove(self.stack.len() - 2);

                    match addr {
                        VMVal::Addr(addr) => self.ptr = addr,
                        _ => {
                            dbg!(addr);
                            return Err(VMErr::Generic);
                        }
                    }

                    continue;
                }
                In::MakeList(len) => {
                    let xs = (0..len).map(|_| self.stack.pop().unwrap()).collect();
                    self.push(VMVal::List(xs));
                }
                In::MakeFunction(addr, params_len) => self.push(VMVal::Fn(addr, params_len)),
                In::DeclareLocal(ident) => {
                    let val = self.pop();

                    // discards are not bound
                    if ident != "_" {
                        self.bind(ident, val);
                    }
                }
                In::Assign(ident) => {
                    let val = self.pop();
                    self.assign(ident, val)?;
                }
                In::ExecShell(cmd) => {
                    use std::process::Command;

                    // TODO: possible support for other platforms
                    if cfg!(target_os = "linux") {
                        let args = if let VMVal::List(args) = self.pop() {
                            args.iter().map(|arg| arg.to_string()).collect::<Vec<_>>()
                        } else {
                            panic!("This shouldn't be happening..");
                        };

                        let status = Command::new(cmd)
                            .args(args)
                            .status()
                            .expect("Failed to execute the process..");

                        if !status.success() {
                            dbg!(status);
                            panic!("Non-zero exit code!");
                        }
                    } else {
                        return Err(VMErr::UnsupportedOs);
                    }
                }
                In::EnvLookup(var, alt_addr) => {
                    if let Ok(var) = std::env::var(var) {
                        self.push(VMVal::Str(var));
                    } else {
                        self.ptr = alt_addr;
                        continue;
                    }
                }
                In::Call(name, args_len) => match self.lookup(name)? {
                    VMVal::Fn(addr, actual_len) if *actual_len == args_len => {
                        self.ptr = *addr;
                        continue;
                    }
                    VMVal::Fn(addr, actual_len) => {
                        return Err(VMErr::WrongNumOfArgs(
                            name.to_string(),
                            *actual_len,
                            args_len,
                        ))
                    }
                    _ => return Err(VMErr::Generic),
                },
                In::CallNative("log", 1) => self.log(),
                In::CallNative("dbg", 1) => self.dbg(),
                In::CallNative("type_of", 1) => self.type_of(),
                In::CallNative("len", 1) => self.len()?,
                In::CallNative("get_args", 0) => self.get_args(),
                In::CallNative("lines", 1) => self.lines()?,
                In::CallNative("zip", 2) => self.zip()?,
                In::CallNative(ident, args_len) => {
                    return Err(VMErr::WrongNumOfArgs(ident.to_string(), 1, args_len))
                }
                In::Lookup(ident) => {
                    let val = self.lookup(ident)?.clone();
                    self.push(val);
                }
                _ => panic!("Unsupported instruction"),
            }

            self.ptr += 1;
        }

        Ok(())
    }
}
