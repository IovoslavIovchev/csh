use super::*;

use pretty_assertions::assert_eq;

fn helper(instrs: &[In<'static>]) -> VM<'static> {
    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert_eq!(Ok(()), res);
    assert_eq!(Vec::<VMVal>::new(), vm.stack);

    vm
}

#[test]
fn primitive_bindings() {
    let instrs = vec![
        In::Push(Val::Num(63)),
        In::DeclareLocal("some_num"),
        In::Push(Val::Str("whatever")),
        In::DeclareLocal("some_string"),
        In::Push(Val::Bool(false)),
        In::DeclareLocal("_"),
    ];

    let vm = helper(&instrs);

    assert_eq!(
        &VMVal::Num(63),
        vm.call_stack[0].bindings.get("some_num").unwrap(),
    );
    assert_eq!(
        &VMVal::Str("whatever".to_owned()),
        vm.call_stack[0].bindings.get("some_string").unwrap(),
    );
    assert_eq!(None, vm.get_value("_"));
    assert_eq!(0, vm.stack.len());
}

#[test]
fn unary_exprs() {
    let instrs = vec![In::Push(Val::Bool(false)), In::UnOp(Op::Neg)];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert!(res.is_ok());
    assert_eq!(vm.stack.len(), 1);
    assert_eq!(vm.stack[0], VMVal::Bool(true));
}

#[test]
fn invalid_unary_exprs() {
    let instrs = vec![In::Push(Val::Num(5)), In::UnOp(Op::Neg)];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert_eq!(Err(VMErr::InvalidUnOp(Op::Neg, "Number")), res);
}

#[test]
fn bin_exprs() {
    let instrs = vec![
        In::Push(Val::Str("x = ")),
        In::Push(Val::Num(58)),
        In::Push(Val::Float(3.0)),
        In::Op(Op::Add),
        In::Push(Val::Float(-9.43)),
        In::Op(Op::Sub),
        In::Push(Val::Num(-4)),
        In::Op(Op::Mul),
        In::Op(Op::Add),
        In::Push(Val::Str(" ?")),
        In::Op(Op::Add),
    ];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert!(res.is_ok());
    assert_eq!(vm.stack.len(), 1);
    assert_eq!(vm.stack[0], VMVal::Str("x = -281.72 ?".to_string()));
}

#[test]
fn invalid_bin_exprs() {
    let instrs = vec![
        In::Push(Val::Bool(false)),
        In::Push(Val::Float(3.0)),
        In::Op(Op::Add),
    ];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert_eq!(Err(VMErr::InvalidBinOp("Boolean", Op::Add, "Float")), res);

    let instrs = vec![
        In::Push(Val::Num(56)),
        In::Push(Val::Str("some string")),
        In::Op(Op::Sub),
    ];

    let res = vm.exec(&instrs);
    assert_eq!(res, Err(VMErr::InvalidBinOp("Number", Op::Sub, "String")));
}

#[test]
fn lists_and_tuples() {
    let instrs = vec![
        In::Push(Val::Num(104)),
        In::Push(Val::Str("key")),
        In::MakeList(2),
        In::DeclareLocal("a_tuple"),
    ];

    let vm = helper(&instrs);

    assert_eq!(
        vm.call_stack[0].bindings.get("a_tuple").unwrap(),
        &VMVal::List(vec![VMVal::Str("key".to_owned()), VMVal::Num(104)])
    );
}

#[test]
fn indexing() {
    let instrs = vec![
        In::Push(Val::Str("zero")),
        In::Push(Val::Num(0)),
        In::MakeList(2),
        In::Push(Val::Num(1)),
        In::Op(Op::At),
    ];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert!(res.is_ok());
    assert_eq!(vm.stack.len(), 1);
    assert_eq!(vm.stack[0], VMVal::Str("zero".to_string()));
}

#[test]
fn invalid_indexing() {
    let instrs = vec![
        In::Push(Val::Str("zero")),
        In::Push(Val::Num(0)),
        In::MakeList(2),
        In::Push(Val::Num(-1)),
        In::Op(Op::At),
    ];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert_eq!(Err(VMErr::NegativeIndexing), res);
    assert_eq!(vm.stack.len(), 0);

    let instrs = vec![
        In::Push(Val::Str("zero")),
        In::Push(Val::Num(0)),
        In::MakeList(2),
        In::Push(Val::Num(15)),
        In::Op(Op::At),
    ];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert_eq!(Err(VMErr::OutOfBoundsIndexing), res);
    assert_eq!(vm.stack.len(), 0);
}

#[test]
fn shell() {
    let instrs = vec![
        In::Push(Val::Str("./extra.tmp")),
        In::MakeList(1),
        In::ExecShell("touch"),
    ];

    let _ = helper(&instrs);

    assert!(std::path::Path::new("./extra.tmp").is_file());
    std::fs::remove_file("./extra.tmp").unwrap();
}

#[test]
fn env_vars() {
    let instrs = vec![
        In::Jump(6),
        In::Jump(4),
        In::Push(Val::Str("vim")),
        In::Jump(5),
        In::EnvLookup("EDITOR_ALT", 2),
        In::Jump(7),
        In::EnvLookup("EDITOR", 1),
        In::DeclareLocal("editor"),
    ];

    std::env::remove_var("EDITOR");
    std::env::set_var("EDITOR_ALT", "vi");

    let vm = helper(&instrs);

    assert_eq!(
        vm.call_stack[0].bindings.get("editor").unwrap(),
        &VMVal::Str("vi".to_owned())
    );
}

#[test]
fn ident_lookups() {
    let instrs = vec![
        In::Push(Val::Str("value")),
        In::DeclareLocal("str"),
        In::Lookup("str"),
    ];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert_eq!(vm.stack.len(), 1);
    assert_eq!(vm.stack[0], VMVal::Str("value".to_string()));
}

#[test]
fn unresolved_idents() {
    let instrs = vec![In::Lookup("unresolved")];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert_eq!(Err(VMErr::UnresolvedIdent("unresolved".to_string())), res);
}

#[test]
fn anon_scopes() {
    let instrs = vec![
        In::Push(Val::Bool(false)),
        In::DeclareLocal("outer"),
        In::Begin,
        In::Push(Val::Bool(true)),
        In::DeclareLocal("g"),
        In::Begin,
        In::Lookup("g"),
        In::UnOp(Op::Neg),
        In::DeclareLocal("g"),
        In::Push(Val::Num(42)),
        In::DeclareLocal("answer"),
        In::End,
        In::Lookup("g"),
        In::Assign("outer"),
        In::End,
    ];

    let vm = helper(&instrs);

    assert_eq!(vm.call_stack.len(), 1);
    assert_eq!(
        vm.call_stack[0].bindings.get("outer").unwrap(),
        &VMVal::Bool(true)
    );
    assert_eq!(vm.call_stack[0].bindings.get("g"), None);
    assert_eq!(vm.call_stack[0].bindings.get("answer"), None);
}

#[test]
fn if_exprs() {
    let instrs = vec![
        In::Push(Val::Bool(false)),
        In::DeclareLocal("cond"),
        In::Lookup("cond"),
        In::JumpIfNot(6),
        In::Push(Val::Num(1)),
        In::Jump(7),
        In::Push(Val::Unit),
    ];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert_eq!(Ok(()), res);
    assert_eq!(vm.stack.len(), 1);
    assert_eq!(vm.stack[0], VMVal::Unit);
    assert_eq!(
        vm.call_stack[0].bindings.get("cond").unwrap(),
        &VMVal::Bool(false)
    );
}

#[test]
fn function_declarations() {
    let instrs = vec![
        In::Jump(9),
        In::DeclareLocal("y"),
        In::DeclareLocal("x"),
        In::Lookup("x"),
        In::Lookup("y"),
        In::Op(Op::Add),
        In::DeclareLocal("res"),
        In::Lookup("res"),
        In::Ret,
        In::MakeFunction(1, 2),
    ];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert_eq!(Ok(()), res);
    assert_eq!(vm.stack.len(), 1);
    assert_eq!(vm.stack[0], VMVal::Fn(1, 2));
}

#[test]
fn function_calling() {
    let instrs = vec![
        In::Jump(11),
        In::Begin,
        In::DeclareLocal("y"),
        In::DeclareLocal("x"),
        In::Lookup("x"),
        In::Lookup("y"),
        In::Op(Op::Add),
        In::DeclareLocal("res"),
        In::Lookup("res"),
        In::End,
        In::Ret,
        In::MakeFunction(1, 2),
        In::DeclareLocal("sum"),
        In::Push(Val::Addr(17)),
        In::Push(Val::Float(9.99)),
        In::Push(Val::Num(2)),
        In::Call("sum", 2),
    ];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert_eq!(Ok(()), res);
    assert_eq!(vm.stack.len(), 1);
    assert_eq!(vm.stack[0], VMVal::Float(11.99));
    assert_eq!(vm.ptr, 17);
}

#[test]
fn function_calling_with_wrong_num_of_args() {
    let instrs = vec![
        In::MakeFunction(0, 2),
        In::DeclareLocal("sum"),
        In::Push(Val::Num(2)),
        In::Call("sum", 1),
    ];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert_eq!(Err(VMErr::WrongNumOfArgs("sum".to_string(), 2, 1)), res);
}

#[test]
fn factorial() {
    let instrs = vec![
        In::Jump(24),
        In::Begin,
        In::DeclareLocal("x"),
        // if x < 0
        In::Lookup("x"),
        In::Push(Val::Num(0)),
        In::Op(Op::LT),
        In::JumpIfNot(9),
        In::Push(Val::Unit),
        In::Jump(22),
        // if x < 2
        In::Lookup("x"), // 9
        In::Push(Val::Num(2)),
        In::Op(Op::LT),
        In::JumpIfNot(15),
        In::Push(Val::Num(1)),
        In::Jump(22),
        // else
        In::Lookup("x"), // 15
        In::Push(Val::Addr(21)),
        In::Lookup("x"),
        In::Push(Val::Num(1)),
        In::Op(Op::Sub),
        In::Call("fact", 1),
        In::Op(Op::Mul), // 21
        In::End,         // 22
        In::Ret,
        In::MakeFunction(1, 1),
        In::DeclareLocal("fact"),
        In::Push(Val::Addr(29)),
        In::Push(Val::Num(5)),
        In::Call("fact", 1),
    ];

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    assert_eq!(Ok(()), res);
    assert_eq!(vm.stack.len(), 1);
    assert_eq!(vm.stack[0], VMVal::Num(120));
}
