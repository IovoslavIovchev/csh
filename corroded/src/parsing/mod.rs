use crate::ast::Expr;

use crate::grammar;

#[cfg(test)]
mod tests;

pub fn parse(s: &str) -> Result<Expr, &'static str> {
    use lalrpop_util::ParseError;

    grammar::ExprsParser::new().parse(s).map_err(|e| {
        dbg!(&e);

        match e {
            ParseError::User { error } => error,
            _ => "Parsing error..",
        }
    })
}
