use super::*;
use crate::ast::{Expr, Op, Val};

use pretty_assertions::assert_eq;

use Expr::*;
use Val::*;

macro_rules! assert_is {
    ($src:expr, ok $expr:expr) => {
        assert_is!($src, Ok($expr))
    };
    ($src:expr, err $err:expr) => {
        assert_is!($src, Err($err))
    };
    ($src:expr, $expr:expr) => {
        let expr = parse($src);
        assert_eq!($expr, expr);
    };
}

#[test]
fn comments() {
    assert_is!(
        r#"
-- single line

/* a comment */

/*
a multiline /* nested */
comment */
"#,
        ok Multiple(vec![])
    );
}

#[test]
fn simple_values() {
    assert_is!(
        "() 1 -99 4.20 True False 'string with a few words124%#@#'",
        ok Multiple(vec![
            Val(Unit),
            Val(Num(1)),
            Val(Num(-99)),
            Val(Float(4.2)),
            Val(Bool(true)),
            Val(Bool(false)),
            Val(Str("string with a few words124%#@#"))
        ])
    );
}

#[test]
fn too_big_numeric_value() {
    assert_is!("999999999999999999999999", err "numeric value too big");
}

#[test]
fn boolean_negation() {
    let expr = parse("~True ~False").unwrap();

    assert_eq!(
        Multiple(vec![
            Un {
                op: Op::Neg,
                expr: Box::new(Val(Bool(true)))
            },
            Un {
                op: Op::Neg,
                expr: Box::new(Val(Bool(false)))
            },
        ]),
        expr
    );
}

#[test]
fn lists_and_tuples() {
    assert_is!(r#"
[ 1, -23, ]

[ 'pi', 3.14 ]
"#,
    ok Multiple(vec![
            List(vec![Val(Num(1)), Val(Num(-23)),]),
            List(vec![Val(Str("pi")), Val(Float(3.14)),])
        ])
    );
}

#[test]
fn indexing() {
    assert_is!(
        r#"
[1,] . (2 * 0)

xs.idx

first = xs.0
    "#,
        ok Multiple(vec![
            Bin {
                l: Box::new(List(vec![Val(Num(1))])),
                op: Op::At,
                r: Box::new(Bin {
                    l: Box::new(Val(Num(2))),
                    op: Op::Mul,
                    r: Box::new(Val(Num(0)))
                })
            },
            Bin {
                l: Box::new(Lookup("xs")),
                op: Op::At,
                r: Box::new(Lookup("idx")),
            },
            Declaration {
                ident: "first",
                val: Box::new(Bin {
                    l: Box::new(Lookup("xs")),
                    op: Op::At,
                    r: Box::new(Val(Num(0))),
                })
            }
        ])
    );
}

#[test]
fn unary_ops() {
    assert_is!(
        "~z",
        ok Multiple(vec![
            Un {
                op: Op::Neg,
                expr: Box::new(Lookup("z")),
            }
        ])
    );
}

#[test]
fn bin_arithmetic_and_precedence() {
    let expr = parse(
        r#"
1 - 1 + 1
1 + 2 * 3
2 * 3 / 4
3 ^ 4 * 5
3 ^ 4 ^ 5
True && ~False || False
'five = ' + (2 * 15 - 25)
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![
            Bin {
                l: Box::new(Bin {
                    l: Box::new(Val(Num(1))),
                    op: Op::Sub,
                    r: Box::new(Val(Num(1))),
                }),
                op: Op::Add,
                r: Box::new(Val(Num(1))),
            },
            Bin {
                l: Box::new(Val(Num(1))),
                op: Op::Add,
                r: Box::new(Bin {
                    l: Box::new(Val(Num(2))),
                    op: Op::Mul,
                    r: Box::new(Val(Num(3))),
                })
            },
            Bin {
                l: Box::new(Bin {
                    l: Box::new(Val(Num(2))),
                    op: Op::Mul,
                    r: Box::new(Val(Num(3))),
                }),
                op: Op::Div,
                r: Box::new(Val(Num(4))),
            },
            Bin {
                l: Box::new(Bin {
                    l: Box::new(Val(Num(3))),
                    op: Op::Pow,
                    r: Box::new(Val(Num(4))),
                }),
                op: Op::Mul,
                r: Box::new(Val(Num(5))),
            },
            Bin {
                l: Box::new(Bin {
                    l: Box::new(Val(Num(3))),
                    op: Op::Pow,
                    r: Box::new(Val(Num(4))),
                }),
                op: Op::Pow,
                r: Box::new(Val(Num(5))),
            },
            Bin {
                l: Box::new(Bin {
                    l: Box::new(Val(Bool(true))),
                    op: Op::And,
                    r: Box::new(Un {
                        op: Op::Neg,
                        expr: Box::new(Val(Bool(false)))
                    }),
                }),
                op: Op::Or,
                r: Box::new(Val(Bool(false))),
            },
            Bin {
                l: Box::new(Val(Str("five = "))),
                op: Op::Add,
                r: Box::new(Bin {
                    l: Box::new(Bin {
                        l: Box::new(Val(Num(2))),
                        op: Op::Mul,
                        r: Box::new(Val(Num(15)))
                    }),
                    op: Op::Sub,
                    r: Box::new(Val(Num(25)))
                })
            },
        ]),
        expr
    );
}

#[test]
fn bin_equality_and_precedence() {
    let expr = parse(
        r#"
3 + 8 == 7 - (-4)

5 * 3 /= 9 * x / 15

True || False && ~False
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![
            Bin {
                l: Box::new(Bin {
                    l: Box::new(Val(Num(3))),
                    op: Op::Add,
                    r: Box::new(Val(Num(8))),
                }),
                op: Op::EQ,
                r: Box::new(Bin {
                    l: Box::new(Val(Num(7))),
                    op: Op::Sub,
                    r: Box::new(Val(Num(-4))),
                }),
            },
            Bin {
                l: Box::new(Bin {
                    l: Box::new(Val(Num(5))),
                    op: Op::Mul,
                    r: Box::new(Val(Num(3))),
                }),
                op: Op::NE,
                r: Box::new(Bin {
                    l: Box::new(Bin {
                        l: Box::new(Val(Num(9))),
                        op: Op::Mul,
                        r: Box::new(Lookup("x")),
                    }),
                    op: Op::Div,
                    r: Box::new(Val(Num(15))),
                }),
            },
            Bin {
                l: Box::new(Val(Bool(true))),
                op: Op::Or,
                r: Box::new(Bin {
                    l: Box::new(Val(Bool(false))),
                    op: Op::And,
                    r: Box::new(Un {
                        op: Op::Neg,
                        expr: Box::new(Val(Bool(false)))
                    })
                }),
            }
        ]),
        expr
    );
}

#[test]
fn variable_declarations() {
    let expr = parse(
        r#"
unit = (())
num = 7
float = -1.23
boolean = ~False
a_string = 'whatever'
tup = [2, 'two']

_ = 'whatever'
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![
            Declaration {
                ident: "unit",
                val: Box::new(Val(Unit))
            },
            Declaration {
                ident: "num",
                val: Box::new(Val(Num(7)))
            },
            Declaration {
                ident: "float",
                val: Box::new(Val(Float(-1.23)))
            },
            Declaration {
                ident: "boolean",
                val: Box::new(Un {
                    op: Op::Neg,
                    expr: Box::new(Val(Bool(false)))
                })
            },
            Declaration {
                ident: "a_string",
                val: Box::new(Val(Str("whatever")))
            },
            Declaration {
                ident: "tup",
                val: Box::new(List(vec![Val(Num(2)), Val(Str("two"))]))
            },
            Declaration {
                ident: "_",
                val: Box::new(Val(Str("whatever")))
            },
        ]),
        expr
    );
}

#[test]
fn variable_assignments() {
    let expr = parse(
        r#"
x = False

x := 5
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![
            Declaration {
                ident: "x",
                val: Box::new(Val(Bool(false)))
            },
            Assignment {
                ident: "x",
                val: Box::new(Val(Num(5)))
            },
        ]),
        expr
    );
}

#[test]
fn shell_invocs() {
    let expr = parse(
        r#"
${ cd [some_dir] }

${ some_other_command ['first', 'second',] }
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![
            ShellInvoc {
                cmd: "cd",
                args: vec![Lookup("some_dir")]
            },
            ShellInvoc {
                cmd: "some_other_command",
                args: vec![Val(Str("first")), Val(Str("second"))]
            }
        ]),
        expr
    );
}

#[test]
fn env_vars() {
    let expr = parse(
        r#"
$EDITOR | $EDITOR_ALT | 'vim'
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![EnvVar {
            var: "EDITOR",
            fallback: Box::new(EnvVar {
                var: "EDITOR_ALT",
                fallback: Box::new(Val(Str("vim")))
            })
        }]),
        expr
    )
}

#[test]
fn anon_scopes() {
    let expr = parse(
        r#"
begin
  g = False

  begin
    g := ~g
    x = 66
  end
end
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![Scope(vec![
            Declaration {
                ident: "g",
                val: Box::new(Val(Bool(false)))
            },
            Scope(vec![
                Assignment {
                    ident: "g",
                    val: Box::new(Un {
                        op: Op::Neg,
                        expr: Box::new(Lookup("g"))
                    })
                },
                Declaration {
                    ident: "x",
                    val: Box::new(Val(Num(66)))
                }
            ])
        ])]),
        expr
    );
}

#[test]
fn if_exprs() {
    let expr = parse(
        r#"
x = if
| cond1  -> 5 - 9,
| x /= 9 -> False,
| _      -> y,
end
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![Declaration {
            ident: "x",
            val: Box::new(If {
                branches: vec![
                    (
                        Lookup("cond1"),
                        Bin {
                            l: Box::new(Val(Num(5))),
                            op: Op::Sub,
                            r: Box::new(Val(Num(9)))
                        }
                    ),
                    (
                        Bin {
                            l: Box::new(Lookup("x")),
                            op: Op::NE,
                            r: Box::new(Val(Num(9)))
                        },
                        Val(Bool(false))
                    )
                ],
                fallback: Some(Box::new(Lookup("y")))
            })
        }]),
        expr
    );
}

#[test]
fn if_with_scopes() {
    let expr = parse(
        r#"
some_cond = False

if
| some_cond -> begin
    ${ echo [ 'some_cond is `True`' ] }
  end,
| _ -> begin
    ${ echo [ 'some_cond is `False`' ] }
  end,
end
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![
            Declaration {
                ident: "some_cond",
                val: Box::new(Val(Bool(false)))
            },
            If {
                branches: vec![(
                    Lookup("some_cond"),
                    Scope(vec![ShellInvoc {
                        cmd: "echo",
                        args: vec![Val(Str("some_cond is `True`"))]
                    }])
                )],
                fallback: Some(Box::new(Scope(vec![ShellInvoc {
                    cmd: "echo",
                    args: vec![Val(Str("some_cond is `False`"))]
                }])))
            }
        ]),
        expr
    );
}

#[test]
fn function_definitions() {
    let expr = parse(
        r#"
function seven is [] begin
  7
end

-- trailing commas are fine
function sum is [x, y,] begin
  res = x + y
  res
end

x = seven $ []
x = sum $ [x, 9]
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![
            FuncDef {
                ident: "seven",
                params: vec![],
                body: vec![Val(Num(7))]
            },
            FuncDef {
                ident: "sum",
                params: vec!["x", "y"],
                body: vec![
                    Declaration {
                        ident: "res",
                        val: Box::new(Bin {
                            l: Box::new(Lookup("x")),
                            op: Op::Add,
                            r: Box::new(Lookup("y"))
                        })
                    },
                    Lookup("res"),
                ]
            },
            Declaration {
                ident: "x",
                val: Box::new(Call {
                    ident: "seven",
                    args: vec![]
                })
            },
            Declaration {
                ident: "x",
                val: Box::new(Call {
                    ident: "sum",
                    args: vec![Lookup("x"), Val(Num(9))]
                })
            },
        ]),
        expr
    );
}

#[test]
fn lambdas() {
    assert_is!(
        r#"
lambda x, y, begin
  x + y
end
"#,
        ok Multiple(vec![
            Lambda {
                params: vec!["x", "y"],
                body: vec![
                    Bin {
                        l: Box::new(Lookup("x")),
                        op: Op::Add,
                        r: Box::new(Lookup("y")),
                    }
                ]
            }
        ])
    );
}

#[test]
fn hofs() {
    assert_is!(
    r#"
f $ [lambda x begin () end]
    "#,

        ok Multiple(vec![
            Call {
                ident: "f",
                args: vec![
                    Lambda {
                        params: vec!["x"],
                        body: vec![Val(Unit)]
                    }
                ]
            }
        ])
    );
}

#[test]
fn function_chaining() {
    let expr = parse(
        r#"
5 + 8 |> f $ [!, 3] >> g $ [y, !]
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![Bin {
            l: Box::new(Bin {
                l: Box::new(Bin {
                    l: Box::new(Val(Num(5))),
                    op: Op::Add,
                    r: Box::new(Val(Num(8)))
                }),
                op: Op::Chain,
                r: Box::new(Call {
                    ident: "f",
                    args: vec![ChainVal, Val(Num(3))]
                })
            }),
            op: Op::Chain,
            r: Box::new(Call {
                ident: "g",
                args: vec![Lookup("y"), ChainVal]
            })
        }]),
        expr
    );
}

#[test]
fn for_loops() {
    let expr = parse(
        r#"
for x, idx in [1, 3] loop
  y := x + y
end
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![Expr::ForLoop(
            "x",
            Some("idx"),
            Box::new(Expr::List(vec![
                Expr::Val(Val::Num(1)),
                Expr::Val(Val::Num(3))
            ])),
            vec![Expr::Assignment {
                ident: "y",
                val: Box::new(Expr::Bin {
                    l: Box::new(Expr::Lookup("x")),
                    op: Op::Add,
                    r: Box::new(Expr::Lookup("y"))
                })
            }]
        )]),
        expr
    );
}

#[test]
fn range_loops() {
    let expr = parse(
        r#"
for idx from 0 to 10 inc by 2 loop
  x := x + idx
end
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![Expr::RangeLoop {
            ident: "idx",
            from: Box::new(Expr::Val(Val::Num(0))),
            down: false,
            to: Box::new(Expr::Val(Val::Num(10))),
            inc: true,
            by: Box::new(Expr::Val(Val::Num(2))),
            body: vec![Expr::Assignment {
                ident: "x",
                val: Box::new(Expr::Bin {
                    l: Box::new(Expr::Lookup("x")),
                    op: Op::Add,
                    r: Box::new(Expr::Lookup("idx"))
                })
            }]
        }]),
        expr
    );
}

#[test]
fn range_loops_down_to() {
    let expr = parse(
        r#"
for idx from 10 down to 1 inc by -2 loop
  x := x + idx
end
"#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![Expr::RangeLoop {
            ident: "idx",
            from: Box::new(Expr::Val(Val::Num(10))),
            down: true,
            to: Box::new(Expr::Val(Val::Num(1))),
            inc: true,
            by: Box::new(Expr::Val(Val::Num(-2))),
            body: vec![Expr::Assignment {
                ident: "x",
                val: Box::new(Expr::Bin {
                    l: Box::new(Expr::Lookup("x")),
                    op: Op::Add,
                    r: Box::new(Expr::Lookup("idx"))
                })
            }]
        }]),
        expr
    );
}

#[test]
fn factorial() {
    let expr = parse(
        r#"
function fact is [x] begin
  if
  | x < 0 -> (),
  | x < 2 -> 1,
  | _     -> x * fact $ [x - 1],
  end
end
    "#,
    )
    .unwrap();

    assert_eq!(
        Multiple(vec![FuncDef {
            ident: "fact",
            params: vec!["x"],
            body: vec![If {
                branches: vec![
                    (
                        Bin {
                            l: Box::new(Lookup("x")),
                            op: Op::LT,
                            r: Box::new(Val(Num(0)))
                        },
                        Val(Unit)
                    ),
                    (
                        Bin {
                            l: Box::new(Lookup("x")),
                            op: Op::LT,
                            r: Box::new(Val(Num(2)))
                        },
                        Val(Num(1))
                    ),
                ],
                fallback: Some(Box::new(Bin {
                    l: Box::new(Lookup("x")),
                    op: Op::Mul,
                    r: Box::new(Call {
                        ident: "fact",
                        args: vec![Bin {
                            l: Box::new(Lookup("x")),
                            op: Op::Sub,
                            r: Box::new(Val(Num(1)))
                        }]
                    })
                }))
            }]
        },]),
        expr
    );
}
