use pretty_assertions::assert_eq;

use corroded::{compile_src, VMErr, VMVal, VM};

use VMVal::*;

macro_rules! exec_assert {
    ( $src:expr, is $res:expr $(, + $ident:expr, is $val:expr )* $(, - $ident_un:expr )* ) => {
        let instrs = compile_src($src).unwrap();

        for (idx, &i) in instrs.iter().enumerate() {
            println!("{}:\t{:?}", idx, i);
        }

        let mut vm = VM::new();
        let res = vm.exec(&instrs);

        assert_eq!($res, res);

        $(
            assert_eq!(
                &$val,
                vm.get_value(&$ident).unwrap()
            );
        )*
        $(
            assert_eq!(
                None,
                vm.get_value(&$ident_un)
            );
        )*
    };
}

#[test]
fn value_assignment_and_arithmetic() {
    exec_assert!(r#"
val1 = 'value 1'
val2 = 'this is value 2'

result = 'this is ' + val1 + ', and ' + val2
    "#,

        is Ok(()),

        + "result", is Str("this is value 1, and this is value 2".to_owned())
    );
}

#[test]
fn numeric_arithmetic() {
    exec_assert!(
        r#"
x = 5.78
y = 420
z = -0.33

result = x * 7 + y / 3 * z
"#,

        is Ok(()),

        + "result", is Float(-5.740000000000002)
    );
}

#[test]
fn boolean_arithmetic() {
    exec_assert!(
        r#"
x = 5.3
y = x - 1.3
z = 9.0

result = x == y + 1.3 && (x == z || z > y)
"#,

        is Ok(()),

        + "result", is Bool(true)
    );
}

#[test]
fn lists() {
    exec_assert!(
        r#"
tupl = [0, 'zero']

zero_num = tupl.0
zero_str = tupl.1
"#,

        is Ok(()),

        + "tupl", is List(vec![Num(0), Str("zero".to_owned())]),
        + "zero_num", is Num(0),
        + "zero_str", is Str("zero".to_owned())
    );
}

#[test]
fn list_sum() {
    exec_assert!(
        r#"
function sum_list is [xs] begin
  sum = 0

  for idx from 0 to len[xs] loop
    sum := sum + xs.idx
  end

  sum
end

some_list = [ 1, 4, 7, -9, 3, ]

sum = sum_list $ [some_list]
"#,

        is Ok(()),

        + "sum", is Num(6),

        - "idx"
    );
}

#[test]
fn anonymous_scopes() {
    exec_assert!(
        r#"
x = 3

begin
  y = 5

  begin
    x = x + 5 -- this `x` is local to this scope and equals `8` but the outer `x` is still `3`
    z = x + y

    -- and now set `z` to something totally random
    z = False
  end
  -- `z` does not exist anymore at this point

  x := y -- this `:=` assigns `y` to the first found `x` (the outermost, in this case)
end
-- `y` does not exist anymore at this point
-- but `x` is `5`
"#,

        is Ok(()),

        + "x", is Num(5)
    );
}

#[test]
fn if_exprs() {
    exec_assert!(
        r#"
some_cond = False
some_other_cond = True

result = () -- uninitialised

if
| (True && some_cond) || some_other_cond -> begin
    result := 8
  end,
| _ -> begin
    result := 0
  end
end

result = if |some_other_cond -> result, |_ -> 15 end
"#,

        is Ok(()),

        + "result", is Num(8)
    );
}

#[test]
fn functions() {
    exec_assert!(
        r#"
function sum_three is [x, y, z,]
begin
  mid = x + z
  mid + y
end

x = sum_three $
    -- totally ugly
    [ 1
    , 2
    , 3
    ]

y = sum_three $ [ /* see it fail */ ]
"#,

        is Err(VMErr::WrongNumOfArgs("sum_three".to_string(), 3, 0)),

        + "x", is Num(6),

        - "y"
    );
}

#[test]
fn lambdas() {
    exec_assert!(
        r#"
function f is [g] begin
  g $ [9]
end

x = f $ [lambda x begin x - 9 end]
"#,

        is Ok(()),

        + "x", is Num(0),

        - "g"
    );
}

#[test]
fn range_loops() {
    exec_assert!(
        r#"
sum = 0

for n from 1 to 5 inc loop
  sum := sum + n
end
"#,

        is Ok(()),

        + "sum", is Num(15),

        - "n"
    );
}

#[test]
fn foldl() {
    exec_assert!(
        r#"
function foldl is [xs, f, init] begin
  for idx from 0  to len[xs] loop
    init := f $ [init, xs.idx]
  end

  init
end

xs = [1, 9, -3, 4]

x = foldl $ [xs, lambda acc, x begin acc + x end, 0]
"#,

        is Ok(()),

        + "x", is Num(11)
    );
}

#[test]
fn factorial() {
    exec_assert!(
        r#"
function fact is [x] begin
  if
  | x < 0 -> (),
  | x < 2 -> 1,
  | _     -> x * fact $ [x - 1],
  end
end

x = fact $ [6]
"#,

        is Ok(()),

        + "x", is Num(720)
    );
}
