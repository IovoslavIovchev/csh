# csh

[![Crate](https://img.shields.io/gitlab/pipeline/IovoslavIovchev/csh?style=for-the-badge)](https://gitlab.com/IovoslavIovchev/csh)
[![Crate](https://img.shields.io/crates/v/csh.svg?style=for-the-badge)](https://crates.io/crates/corroded)
[![Crate](https://img.shields.io/crates/l/csh?style=for-the-badge)](https://crates.io/crates/corroded)

The corroded shell language aims to be a simple declarative scripting language and address some of the issues I have with Bash -- for instance, working with and piping structured data adequately. 
It [JIT](https://en.wikipedia.org/wiki/Just-in-time_compilation) compiles to bytecode and runs on its own virtual machine. 
It can also be used as a simple embeddable programming language.

> `csh` is currently in development and only supports a subset of the planned syntax.

## Getting started
The easiest way to get started is to just directly use the `csh` binary. 
Try out some of the examples in.. well, `./examples/`.
```sh
$ cargo run -- ./examples/oi
```

## Language Design

### Basic syntax
The simplest syntax is the value assignment syntax.
```csh
num = 1
float = 5.73
cond = ~False -- `~` is boolean negation
str = 'something..

strings can be multiline, btw'
xs = [1, 2, False] -- lists are dynamically typed so can be used as both lists and tuples

-- values within a list can be accessed as `.<index>`
x = xs.1
```
Values can be either _primitive_ or _compound_.

Comments are prefixed with `--`. Multiline comments are surrounded by `/*` and `*/`.
```csh
-- single line comment

/*
 * a multiline
 * comment
 */
```

### Logic branching
`if`s are in the form of a _chain_, where each branch is specified as `<condition> -> <branch_logic>`. 
`else` is written as `_`. If omitted, an explicit one is added (`_ -> ()`). 
Preferably, each branch should return the same value. 

```csh
if
| num == 3 ||
  num == -3 -> 'num is either 3 or -3',
| _ -> 'num is neither 3, nor -3', -- <- trailing commas are fine
end
```

**TODO:** `switch` / `match`

### Anonymous Scopes
```csh
x = 3

begin
  y = 5

  begin
    x = x + 5 -- this `x` is local to this scope and equals `8` but the outer `x` is still `3`
    z = x + y
  end
  -- `z` does not exist anymore at this point

  x := y -- this `:=` assigns `y` to the first found `x` (the outermost, in this case)
end
-- `y` does not exist anymore at this point
-- but `x` is `5`
```

### Functions
```csh
function f is [x, y, z] begin
  temp = x * 8 - 3 * y
  temp + z
end

x = f $ [1, 2, 3,]
```
The parameter list can have a trailing comma.

By convention, indentation is 2 spaces.

Functions can be called only _after_ they have been declared. Calling functions with the wrong number of arguments is _illegal_, so `f $ [1, 2]` will `panic`.

> While the function parameter list looks like a list value, it is important to note that it is _not_ a list value.

Functions are also first-class citizens, so something like this can be done:
```csh
g = f

x = g $ [5, 8, 12]
```

### Function chaining
```csh
x = f $ [1, 1, 1]
  >> double $ [!]
  >> f $ [8, !, 9]
  |> double $ [!]
```

`!` is the previous computation's return value. It is valid only within a chain. 

`>>` is the same as `|>`, just a matter of preference..

The previous example is equivalent to:
```csh
x = f $ [1, 1, 1]
x = double $ [x]
x = f $ [8, x, 9]
x = double $ [x]
```

### Environment variables
Environment variables can be assigned to `csh` values but they _always_ need to contain a fallback. In case the environment variable is not present the fallback will be lazily evaluated.

```csh
editor = $EDITOR | 'vim'

-- the fallback can be another environment variable
editor = $EDITOR | $EDITOR_ALT | 'vim'
```

### Shell commands
Shell commands can be issued by surrounding them with `${ }`. Usage is: `${ <cmd> [<args>] }`

> if `cmd` is a variable name, `cmd`'s value `.to_string()`ed will be used instead. All arguments will be implicitly converted `.to_string()`

If the status code of the executed command is unsuccessful, the script's execution will be terminated

```csh
${ cd ['~'] }
```

### Loops
The basic "from-to" loop (also includes keywords like `by`, `down` and `inc`)

```csh
${ echo [ 'Countdown' ] }
for i from 10 down to 0 inc loop
  ${ echo [ i ] }
end
```

..or the "iterator" loop

```csh
for i in [1..10] loop
  ${ echo [ 'Iteration', i ] }
end
```

..or the while-loop equivalent

```csh
if some_condition loop
  ${ echo [ 'Looping..' ] }
end
```

..or just an endless loop

```csh
loop
  ${ echo [ 'Endlessly..' ] }
end
```

### Modules
Modules are other `csh` files that can be `use`d. `prelude` is a special (and the only) _built-in_ module.
```csh
use prelude as p

p.echo 'Echoing with `prelude.echo`'
```
Local files can also be used.
```csh
-- this imports `./dir/file' eagerly
use dir.file as f
```
