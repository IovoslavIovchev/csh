use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::PathBuf;

use corroded::{compile_src, prettify_err, VM};
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "csh")]
struct Opt {
    /// File to execute
    #[structopt(name = "file", parse(from_os_str))]
    file: Option<PathBuf>,

    /// Command to execute
    #[structopt(short = "c")]
    cmd: Option<String>,
}

fn main() -> std::io::Result<()> {
    let opt = Opt::from_args();

    let src = if let Some(file) = opt.file {
        dbg!(&file);
        let file = File::open(file)?;
        let mut buf_reader = BufReader::new(file);
        let mut contents = String::new();
        buf_reader.read_to_string(&mut contents)?;
        contents
    } else if let Some(cmd) = opt.cmd {
        dbg!(&cmd);
        cmd
    } else {
        return Ok(());
    };

    let instrs = compile_src(&src)
        .map_err(|e| dbg!(e))
        .map_or_else(|_| vec![], |v| v);

    let mut vm = VM::new();
    let res = vm.exec(&instrs);

    if let Err(err) = res {
        eprintln!("{}", prettify_err(err));
        std::process::exit(1);
    }

    Ok(())
}
